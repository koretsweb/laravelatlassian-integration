<?php
return [
    'hipchat' => [
        'url' => 'https://boldendeavours.hipchat.com/v2/room/{roomId}/notification?auth_token={token}'
    ]
];