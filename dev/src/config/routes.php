<?php

Route::group(['prefix' => LaravelLocalization::setLocale()],
    function () {
        Route::group(
            [
                'prefix' => 'development',
            ],
            function () {
                Route::group(['prefix' => 'bitbucket', 
                        'namespace' => '\BE\Dev\Backend\Http\Controllers'], function () {
                    Route::post('/', [
                        'as' => 'bitbucket.push_event',
                        'uses' => 'BitbucketEventsController@pushEvent'
                    ]);
                });
            }
        );
    }
);

