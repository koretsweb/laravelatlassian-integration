<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 08.10.17
 * Time: 17:12
 */

namespace BE\Dev;


use BE\Core\Module\AbstractServiceProvider;

class DevServiceProvider extends AbstractServiceProvider
{
    public static function getPackageName()
    {
        return 'be_dev';
    }

    public function boot()
    {
        parent::boot();

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path('be_dev.php')
        ], 'config');
    }
}