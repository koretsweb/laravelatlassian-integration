<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 09.10.17
 * Time: 8:47
 */

namespace BE\Dev\Services\HipChat;


class HipChatService
{

    /**
     * @var $config array Service config
     */
    protected $config;

    public function __construct()
    {
        $this->config = config('be_dev.hipchat');
    }
    public function sendNotification($data)
    {
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $this->config['url']);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        
        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        return $output;
    }
}