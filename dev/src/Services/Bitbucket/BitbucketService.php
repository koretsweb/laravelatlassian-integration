<?php

namespace BE\Dev\Services\Bitbucket;

class BitbucketService
{
    /**
     * @var $data array sent from bitbucket cloud
     */
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }
}