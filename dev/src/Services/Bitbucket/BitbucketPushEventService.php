<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 26.10.17
 * Time: 19:51
 */

namespace BE\Dev\Services\Bitbucket;


class BitbucketPushEventService extends BitbucketService
{
    public function getBranch()
    {
        $branch = $this->data['push']['changes'][0]['new']['name'];

        return $branch;
    }
}