<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 26.10.17
 * Time: 21:19
 */

namespace BE\Dev\Services;

use BE\Dev\Services\HipChat\HipChatService;
use Illuminate\Bus\Queueable;
use BE\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RuntTestsInQueueAndNotifyHipChatRoom extends Job implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var $deployService DeployService
     */
    protected $deployService;

    /**
     * @var $tests RunTests
     */
    protected $tests;

    /**
     * @var $hipchatService HipChatService
     */
    protected $hipchatService;

    public function __construct()
    {
        $this->deployService = app(DeployService::class);
        $this->tests = app(RunTests::class);
        $this->tests = app(HipChatService::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->deployService->pullPackagesChanges();

            $outputTests = $this->tests->run();

            if ($outputTests === true) {
                $outputTests = 'Тесты прошли успешно';
                $colorTests = 'green';
            } else {
                $colorTests = 'red';
            }

            $this->hipchatService->sendNotification([
                'color' => $colorTests,
                'message' => $outputTests,
                'notify'   => true,
                'message_format' => 'html',
            ]);

        } catch (\Exception $exception) {
            \Log::error($exception->getMessage());
        }
    }
}