<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 24.10.17
 * Time: 12:22
 */

namespace BE\Dev\Services;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class DeployService
{
    public function pullPackagesChanges()
    {
        $process = new Process('cd ../packages/be && git pull');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }
}