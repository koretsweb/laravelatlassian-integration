<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 08.10.17
 * Time: 21:18
 */

namespace BE\Dev\Services;


use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RunTests
{
    /**
     * @return string
     */
    public function run()
    {
        $process = new Process('cd ../ && vendor/bin/phpunit packages/be/statistics --tap');
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            return $process->getIncrementalOutput();
        }

        return true;
    }
}