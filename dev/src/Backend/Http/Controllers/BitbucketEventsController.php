<?php
/**
 * Created by PhpStorm.
 * User: dmytro
 * Date: 08.10.17
 * Time: 17:17
 */

namespace BE\Dev\Backend\Http\Controllers;


use App\Http\Controllers\Controller;
use BE\Dev\Services\Bitbucket\BitbucketPushEventService;
use BE\Dev\Services\HipChat\HipChatService;
use BE\Dev\Services\JobDispatcher;
use Illuminate\Http\Request;

class BitbucketEventsController extends Controller
{
    /**
     * @var $hipchatService HipChatService
     */
    protected $hipchatService;

    /**
     * @var $pushService BitbucketPushEventService
     */
    protected $pushService;

    protected $config;

    /**
     * @var JobDispatcher
     */
    protected $dispatcher;

    public function __construct(Request $request)
    {
        $this->config = config('be_dev');
        $this->hipchatService = app(HipChatService::class);
        $this->pushService = app(BitbucketPushEventService::class, [$request->all()]);
    }
    public function pushEvent(Request $request)
    {
        $data = $request->all();

        if ($this->pushService->getBranch() != 'staging') {
            return false;
        }

        $comment = strtolower($data['push']['changes'][0]['commits'][0]['message']);
        $author = $data['actor']['display_name'];

        $data = [
            'color' => 'green',
            'message' => "<strong>{$author}</strong> только что запушил в репозиторий BE с комментарием \"{$comment}\"",
            'notify'   => true,
            'message_format' => 'html',
        ];

        $this->hipchatService->sendNotification($data);

        if (strpos($comment, 'no tests')) {
            return response()->json([
                'success' => true,
                'message' => 'tests was not executed'
            ])->setStatusCode(200);
        }

        $service = new \BE\Dev\Services\RuntTestsInQueueAndNotifyHipChatRoom();
        $service->handle();
        
        return response()->json([
            'success' => true
        ])->setStatusCode(200);
    }
}